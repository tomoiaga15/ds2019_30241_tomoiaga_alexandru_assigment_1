package SD;
import SD.controller.UserController;
import SD.dto.builders.ActivityBuilder;
import SD.dto.builders.PatientBuilder;
import SD.entities.Activity;
import SD.entities.ActivityModel;
import SD.services.ActivityService;
import SD.services.PatientService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;

@Component
public class Reciever implements CommandLineRunner {
    private final static String QUEUE_NAME = "activity";

    @Autowired
    private final ActivityService activityService;
    @Autowired
    private final PatientService patientService;

    @Autowired
    private UserController userController;

    public Reciever(ActivityService activityService, PatientService patientService) {
        this.activityService = activityService;
        this.patientService = patientService;
    }

    @Override
    public void run(String... args) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");



        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");
            System.out.println(" [x] Received '" + message + "'");
            final SimpleDateFormat formatter =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            ActivityModel activityModel = mapper.readValue(message, ActivityModel.class);
            Activity activity = new Activity();
            activity.setActivityName(activityModel.getActivityName());
            activity.setPatient(PatientBuilder.generateEntityFromDTO(patientService.findById(activityModel.getPatientId())));
            try {
                activity.setStartTime(formatter.parse(activityModel.getStartTime()));
                activity.setEndTime(formatter.parse(activityModel.getEndTime()));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            activity.setNormal("yes");

            if ( activity.getEndTime().getTime() - activity.getStartTime().getTime() > 360000 && (activity.getActivityName().equals("Showering") || activity.getActivityName().equals("Toileting"))) {
                System.out.println("Alert 1!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                activity.setNormal("Toilet");
                try {
                    userController.alerting("Too much bathroom for id: " + activity.getPatient().getId());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if ( activity.getEndTime().getTime() - activity.getStartTime().getTime() > 43200000 && activity.getActivityName().equals("Leaving")) {
                System.out.println("Alert 2!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                activity.setNormal("Leave");
                try {
                    userController.alerting("Too much leaving for id: " + activity.getPatient().getId());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if ( activity.getEndTime().getTime() - activity.getStartTime().getTime() > 43200000 && activity.getActivityName().equals("Sleeping")) {
                System.out.println("Alert 3!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                activity.setNormal("Sleep");
                try {
                    userController.alerting("Too much sleeping for id: " + activity.getPatient().getId());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            activity.setRecommendations("none");
            activity.setTime((int) (activity.getEndTime().getTime() - activity.getStartTime().getTime()));
            activityService.insert(ActivityBuilder.generateDTOFromEntity(activity));
        };
        channel.basicConsume(QUEUE_NAME, true, deliverCallback, consumerTag -> { });
    }
}
