package SD.repositories;

import SD.entities.Medication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface MedicationRepository extends JpaRepository<Medication, Integer> {

    Medication findByName(String name);
}
