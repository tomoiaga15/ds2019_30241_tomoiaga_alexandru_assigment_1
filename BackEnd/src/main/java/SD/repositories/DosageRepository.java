package SD.repositories;

import SD.entities.Dosage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface DosageRepository extends JpaRepository<Dosage, Integer> {

    @Query(value = "SELECT d " +
            "FROM Dosage d " +
            "WHERE d.medicationPerPlan.id = ?1")
    List<Dosage> getDosageForMPId(Integer id);
}
