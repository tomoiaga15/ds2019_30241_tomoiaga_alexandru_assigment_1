package SD.repositories;

import SD.entities.Intake;
import SD.entities.User;
import SD.entities.enums.IntakeMoment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface IntakeRepository extends JpaRepository<Intake, Integer> {

    @Query(value = "SELECT i " +
            "FROM Intake i " +
            "WHERE i.intakeMoment = ?1")
    Intake findByMoment(IntakeMoment moment);
}
