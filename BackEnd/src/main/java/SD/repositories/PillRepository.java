package SD.repositories;

import SD.entities.PillTake;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface PillRepository extends JpaRepository<PillTake, Integer> {
}
