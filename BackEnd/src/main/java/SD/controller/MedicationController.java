package SD.controller;

import SD.dto.MedicationDTO;
import SD.services.MedicationService;
import org.omg.PortableInterceptor.ORBInitInfoPackage.DuplicateName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/medication")
public class MedicationController {
    
    private final MedicationService medicationService;
    
    @Autowired
    public MedicationController(MedicationService medicationService) {
        this.medicationService = medicationService;
    }

    @GetMapping()
    public List<MedicationDTO> findAll(){
        return medicationService.findAll();
    }

    @GetMapping(value = "/{id}")
    public MedicationDTO findById(@PathVariable("id") Integer id){
        return medicationService.findById(id);
    }

    @PostMapping()
    public Integer insert(@Valid @RequestBody MedicationDTO medicationDTO) throws DuplicateName {
        return medicationService.insert(medicationDTO);
    }

    @PutMapping()
    public Integer update(@Valid @RequestBody MedicationDTO medicationDTO){
        return medicationService.update(medicationDTO);
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable("id") Integer id){
        medicationService.delete(id);
    }
}
