package SD.controller;

import SD.dto.DosageDTO;
import SD.services.DosageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/dosage")
public class DosageController {

    private final DosageService dosageService;

    @Autowired
    public DosageController(DosageService dosageService) {
        this.dosageService = dosageService;
    }

    @GetMapping()
    public List<DosageDTO> findAll(){
        return dosageService.findAll();
    }

    @GetMapping(value = "/{id}")
    public DosageDTO findById(@PathVariable("id") Integer id){
        return dosageService.findById(id);
    }

    @GetMapping(value = "/mp/{id}")
    public List<DosageDTO> getDosageForMPId(@PathVariable("id") Integer id){
        return dosageService.getDosageForMPId(id);
    }

    @PostMapping()
    public Integer insert(@Valid @RequestBody DosageDTO dosageDTO) {
        return dosageService.insert(dosageDTO);
    }

    @PutMapping()
    public Integer update(@Valid @RequestBody DosageDTO dosageDTO){
        return dosageService.update(dosageDTO);
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable("id") Integer id){
        dosageService.delete(id);
    }
}
