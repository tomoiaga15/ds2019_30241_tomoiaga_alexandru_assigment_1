package SD.controller;

import SD.dto.MedicationPerPlanDTO;
import SD.services.MedicationPerPlanService;
import org.omg.PortableInterceptor.ORBInitInfoPackage.DuplicateName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/medicationperplan")
public class MedicationPerPlanController {

    private final MedicationPerPlanService medicationPerPlanService;

    @Autowired
    public MedicationPerPlanController(MedicationPerPlanService medicationPerPlanService) {
        this.medicationPerPlanService = medicationPerPlanService;
    }

    @GetMapping()
    public List<MedicationPerPlanDTO> findAll(){
        return medicationPerPlanService.findAll();
    }

    @GetMapping(value = "/{id}")
    public MedicationPerPlanDTO findById(@PathVariable("id") Integer id){
        return medicationPerPlanService.findById(id);
    }

    @GetMapping(value = "/mp/{id}")
    public List<MedicationPerPlanDTO> findByMedicationPlanId(@PathVariable("id") Integer id){
        return medicationPerPlanService.findByMedicationPlanId(id);
    }

    @PostMapping()
    public Integer insert(@Valid @RequestBody MedicationPerPlanDTO medicationPerPlanDTO) throws DuplicateName {
        return medicationPerPlanService.insert(medicationPerPlanDTO);
    }

    @PutMapping()
    public Integer update(@Valid @RequestBody MedicationPerPlanDTO medicationPerPlanDTO){
        return medicationPerPlanService.update(medicationPerPlanDTO);
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable("id") Integer id){
        medicationPerPlanService.delete(id);
    }
}
