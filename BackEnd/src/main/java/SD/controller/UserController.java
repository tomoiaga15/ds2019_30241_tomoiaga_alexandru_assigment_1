package SD.controller;

import SD.dto.UserDTO;
import SD.dto.UserViewDTO;
import SD.services.UserService;
import org.omg.PortableInterceptor.ORBInitInfoPackage.DuplicateName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/user")
public class UserController {

    private final UserService userService;
    private final SimpMessagingTemplate simpMessagingTemplate;

    @Autowired
    public UserController(UserService userService, SimpMessagingTemplate simpMessagingTemplate) {
        this.userService = userService;
        this.simpMessagingTemplate = simpMessagingTemplate;
    }

    @GetMapping()
    public List<UserViewDTO> findAll(){
        return userService.findAll();
    }

    @GetMapping(value = "/login/{username}")
    public UserDTO findByName(@PathVariable String username) {
        return userService.findByUsername(username);
    }

    @GetMapping(value = "/{id}")
    public UserDTO findById(@PathVariable("id") Integer id){
        return userService.findById(id);
    }

    @PostMapping()
    public Integer insert(@Valid @RequestBody UserDTO userDTO) throws DuplicateName {
        return userService.insert(userDTO);
    }

    @PutMapping()
    public Integer update(@Valid @RequestBody UserDTO userDTO){
        return userService.update(userDTO);
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable("id") Integer id){
        userService.delete(id);
    }

    @MessageMapping("/message")
    public String alerting(String message) throws Exception{
        simpMessagingTemplate.convertAndSend("/topic/alert",message);
        return message;
    }
}
