package SD.controller;

import SD.dto.RoleDTO;
import SD.services.RoleService;
import org.omg.PortableInterceptor.ORBInitInfoPackage.DuplicateName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/role")
public class RoleController {
    
    private final RoleService roleService;

    @Autowired
    public RoleController(RoleService roleService) {
        this.roleService = roleService;
    }

    @GetMapping()
    public List<RoleDTO> findAll(){
        return roleService.findAll();
    }

    @GetMapping(value = "/{id}")
    public RoleDTO findById(@PathVariable("id") Integer id){
        return roleService.findById(id);
    }

    @PostMapping()
    public Integer insert(@Valid @RequestBody RoleDTO roleDTO) throws DuplicateName {
        return roleService.insert(roleDTO);
    }

    @PutMapping()
    public Integer update(@Valid @RequestBody RoleDTO roleDTO){
        return roleService.update(roleDTO);
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable("id") Integer id){
        roleService.delete(id);
    }
}
