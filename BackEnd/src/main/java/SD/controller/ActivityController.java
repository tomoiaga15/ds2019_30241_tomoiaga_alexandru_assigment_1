package SD.controller;


import SD.dto.ActivityDTO;
import SD.entities.Activity;
import SD.services.ActivityService;
import SD.soap.SOAPConnector;
import SD.spring_boot_soap_example.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@CrossOrigin
@RequestMapping(value = "/activity")
public class ActivityController {

    @Autowired
    private final SOAPConnector soapConnector;

    @Autowired
    private final ActivityService activityService;

    public ActivityController(SOAPConnector soapConnector, ActivityService activityService) {
        this.soapConnector = soapConnector;
        this.activityService = activityService;
    }

    @GetMapping(value = "/{id}")
    public ActivityDTO findById(@PathVariable("id") Integer id){
        return activityService.findById(id);
    }

    @GetMapping()
    public List<ActivityDTO> findAll(){
        return activityService.findAll();
    }

    @GetMapping(value = "/graph/{name}")
    public GetUserResponse findAll(@PathVariable("name") String name){
//        String name = "2019-12-11";//Default Name
        System.out.println(name);
        GetUserRequest request = new GetUserRequest();
        request.setName(name);
        GetUserResponse response =(GetUserResponse) soapConnector.callWebService("http://localhost:8091/soapWS", request);
        System.out.println("Got Response As below ========= : ");
        System.out.println("Name : " +response.getActivity().toString());
        return response;
    }

    @GetMapping(value = "/pills/{name}")
    public GetMedicationResponse findAllPills(@PathVariable("name") String name){
//        String name = "2019-12-11";//Default Name
        GetMedicationRequest request = new GetMedicationRequest();
        request.setName(name);
        GetMedicationResponse response =(GetMedicationResponse) soapConnector.callWebService("http://localhost:8091/soapWS", request);
        System.out.println("Got Response As below ========= : ");
        System.out.println("Name : " +response.getActivity().toString());
        return response;
    }

    @GetMapping(value = "/activity/{name}")
    public GetActivitiesResponse findAllAnormal(@PathVariable("name") String name){
        // String name = "2019-12-11";//Default Name
        GetActivitiesRequest request = new GetActivitiesRequest();
        request.setName(name);
        GetActivitiesResponse response =(GetActivitiesResponse) soapConnector.callWebService("http://localhost:8091/soapWS", request);
        System.out.println("Got Response As below ========= : ");
        System.out.println("Name : " +response.getActivity().toString());
        return response;
    }

    @GetMapping(value = "/recommend")
    public GetRecommendationResponse findAllAnormalRecommendation(){
        String name = "2019-12-11";//Default Name
        GetRecommendationRequest request = new GetRecommendationRequest();
        request.setName(name);
        GetRecommendationResponse response =(GetRecommendationResponse) soapConnector.callWebService("http://localhost:8091/soapWS", request);
        System.out.println("Got Response As below ========= : ");
        System.out.println("Name : " +response.getActivity().toString());
        return response;
    }

    @PutMapping(value = "/normal/{id}")
    public InsertAnormalResponse updateNormal(@PathVariable("id") Integer id, @Valid @RequestBody String activity){

        System.out.println("aici boss:");
        System.out.println(activity);
        InsertAnormal request = new InsertAnormal();
        request.setName(activity);
        request.setId(id);

        InsertAnormalResponse response = (InsertAnormalResponse) soapConnector.callWebService("http://localhost:8091/soapWS", request);

        return response;
    }

    @PutMapping(value = "/recommend/{id}")
    public InsertRecommendationResponse updateRecommend(@PathVariable("id") Integer id, @Valid @RequestBody String activity){

        System.out.println(activity);
        System.out.println("aici ma");
        InsertRecommendation request = new InsertRecommendation();
        request.setName(activity);
        request.setId(id);

        InsertRecommendationResponse response = (InsertRecommendationResponse) soapConnector.callWebService("http://localhost:8091/soapWS", request);

        return response;
    }

}
