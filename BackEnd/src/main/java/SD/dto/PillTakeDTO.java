package SD.dto;

import SD.entities.Intake;
import SD.entities.Medication;
import SD.entities.Patient;

import java.util.Date;
import java.util.Objects;

public class PillTakeDTO {

    private Integer id;
    private Medication medication;
    private Patient patient;
    private Intake intake;
    private String taken;
    private Date date;

    public PillTakeDTO(Integer id, Medication medication, Patient patient, Intake intake, String taken, Date date) {
        this.id = id;
        this.medication = medication;
        this.patient = patient;
        this.intake = intake;
        this.taken = taken;
        this.date = date;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Medication getMedication() {
        return medication;
    }

    public void setMedication(Medication medication) {
        this.medication = medication;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Intake getIntake() {
        return intake;
    }

    public void setIntake(Intake intake) {
        this.intake = intake;
    }

    public String getTaken() {
        return taken;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PillTakeDTO that = (PillTakeDTO) o;
        return id.equals(that.id) &&
                medication.equals(that.medication) &&
                patient.equals(that.patient) &&
                intake.equals(that.intake) &&
                taken.equals(that.taken);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, medication, patient, intake, taken);
    }

    public void setTaken(String taken) {
        this.taken = taken;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
