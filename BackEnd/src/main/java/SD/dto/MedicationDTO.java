package SD.dto;

import java.util.Objects;

public class MedicationDTO {
    private Integer id;
    private String name;
    private String sideEfect;

    public MedicationDTO(Integer id, String name, String sideEfect) {
        this.id = id;
        this.name = name;
        this.sideEfect = sideEfect;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSideEfect() {
        return sideEfect;
    }

    public void setSideEfect(String sideEfect) {
        this.sideEfect = sideEfect;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicationDTO that = (MedicationDTO) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(sideEfect, that.sideEfect);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, sideEfect);
    }
}
