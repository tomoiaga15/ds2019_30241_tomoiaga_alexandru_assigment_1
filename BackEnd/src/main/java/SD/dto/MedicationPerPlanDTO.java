package SD.dto;

import SD.entities.*;

import java.util.Objects;

public class MedicationPerPlanDTO {

    private Integer id;
    private MedicationPlan medicationPlan;
    private Medication medication;

    public MedicationPerPlanDTO(Integer id, MedicationPlan medicationPlan, Medication medication) {
        this.id = id;
        this.medicationPlan = medicationPlan;
        this.medication = medication;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public MedicationPlan getMedicationPlan() {
        return medicationPlan;
    }

    public void setMedicationPlan(MedicationPlan medicationPlan) {
        this.medicationPlan = medicationPlan;
    }

    public Medication getMedication() {
        return medication;
    }

    public void setMedication(Medication medication) {
        this.medication = medication;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicationPerPlanDTO that = (MedicationPerPlanDTO) o;
        return id.equals(that.id) &&
                medicationPlan.equals(that.medicationPlan) &&
                medication.equals(that.medication);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, medicationPlan, medication);
    }
}
