package SD.dto.builders;

import SD.dto.MedicationDTO;
import SD.entities.Medication;

public class MedicationBuilder {

    public MedicationBuilder() {
    }

    public static MedicationDTO generateDTOFromEntity(Medication medication){
        return new MedicationDTO(
                medication.getId(),
                medication.getName(),
                medication.getSideEfect());
    }

    public static Medication generateEntityFromDTO(MedicationDTO medicationDTO){
        return new Medication(
                medicationDTO.getId(),
                medicationDTO.getName(),
                medicationDTO.getSideEfect());
    }
}
