package SD.dto.builders;

import SD.dto.PatientDTO;
import SD.entities.Patient;

public class PatientBuilder {

    public PatientBuilder() {
    }

    public static PatientDTO generateDTOFromEntity(Patient patient){
        return new PatientDTO(
                patient.getId(),
                patient.getUser(),
                patient.getDoctor(),
                patient.getCaregiver(),
                patient.getMedicalRecord());
    }

    public static Patient generateEntityFromDTO(PatientDTO patientDTO){
        return new Patient(
                patientDTO.getId(),
                patientDTO.getUser(),
                patientDTO.getDoctor(),
                patientDTO.getCaregiver(),
                patientDTO.getMedicalRecord());
    }

}
