package SD.dto.builders;

import SD.dto.MedicationPlanDTO;
import SD.entities.MedicationPlan;

public class MedicationPlanBuilder {

    public MedicationPlanBuilder() {
    }

    public static MedicationPlanDTO generateDTOFromEntity(MedicationPlan medicationPlan){
        return new MedicationPlanDTO(
                medicationPlan.getId(),
                medicationPlan.getPatient(),
                medicationPlan.getStartTime(),
                medicationPlan.getEndTime());
    }

    public static MedicationPlan generateEntityFromDTO(MedicationPlanDTO medicationPlanDTO){
        return new MedicationPlan(
                medicationPlanDTO.getId(),
                medicationPlanDTO.getPatient(),
                medicationPlanDTO.getStartTime(),
                medicationPlanDTO.getEndTime());
    }
}
