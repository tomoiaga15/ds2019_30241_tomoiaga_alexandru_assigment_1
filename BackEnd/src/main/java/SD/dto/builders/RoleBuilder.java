package SD.dto.builders;

import SD.dto.RoleDTO;
import SD.entities.Role;

public class RoleBuilder {

    public RoleBuilder() {
    }

    public static RoleDTO generateDTOFromEntity(Role role){
        return new RoleDTO(
                role.getId(),
                role.getType());
    }

    public static Role generateEntityFromDTO(RoleDTO roleDTO){
        return new Role(
                roleDTO.getId(),
                roleDTO.getType());
    }
}
