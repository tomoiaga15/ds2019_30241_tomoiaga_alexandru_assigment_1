package SD.dto.builders;

import SD.dto.PillTakeDTO;
import SD.entities.PillTake;

public class PillTakeBuilder {

    public PillTakeBuilder() {
    }

    public static PillTakeDTO generateDTOFromEntity(PillTake pillTake){
        return new PillTakeDTO(
                pillTake.getId(),
                pillTake.getMedication(),
                pillTake.getPatient(),
                pillTake.getIntake(),
                pillTake.getTaken(),
                pillTake.getDate());
    }
    public static PillTake generateEntityFromDTO(PillTakeDTO pillTakeDTO){
        return new PillTake(
                pillTakeDTO.getId(),
                pillTakeDTO.getMedication(),
                pillTakeDTO.getPatient(),
                pillTakeDTO.getIntake(),
                pillTakeDTO.getTaken(),
                pillTakeDTO.getDate());
    }
}
