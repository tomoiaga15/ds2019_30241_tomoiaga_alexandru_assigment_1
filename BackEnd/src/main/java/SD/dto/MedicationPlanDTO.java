package SD.dto;

import SD.entities.Patient;
import java.util.Date;
import java.util.Objects;

public class MedicationPlanDTO {

    private Integer id;
    private Patient patient;
    private Date startTime;
    private Date endTime;

    public MedicationPlanDTO(Integer id, Patient patient, Date startTime, Date endTime) {
        this.id = id;
        this.patient = patient;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicationPlanDTO that = (MedicationPlanDTO) o;
        return id.equals(that.id) &&
                Objects.equals(patient, that.patient) &&
                Objects.equals(startTime, that.startTime) &&
                Objects.equals(endTime, that.endTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, patient, startTime, endTime);
    }
}
