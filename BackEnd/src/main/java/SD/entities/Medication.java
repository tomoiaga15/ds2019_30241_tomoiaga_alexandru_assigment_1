package SD.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
public class Medication {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "name", nullable = false, length = 100)
    private String name;

    @JsonProperty("sideEfect")
    @Column(name = "side_efect", nullable = false, length = 200)
    private String sideEfect;

    public Medication() {}

    public Medication(Integer id, String name, String sideEfect) {
        this.id = id;
        this.name = name;
        this.sideEfect = sideEfect;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSideEfect() {
        return sideEfect;
    }

    public void setSideEfect(String sideEfect) {
        this.sideEfect = sideEfect;
    }
}
