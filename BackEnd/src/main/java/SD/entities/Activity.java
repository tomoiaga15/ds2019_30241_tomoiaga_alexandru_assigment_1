package SD.entities;

import javax.persistence.*;

import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
public class Activity {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "patient_id")
    private Patient patient;

    @Temporal(TemporalType.DATE)
    @Column(name = "start_time", nullable = false)
    private Date startTime;

    @Temporal(TemporalType.DATE)
    @Column(name = "end_time", nullable = false)
    private Date endTime;

    @Column(name = "activity_name", nullable = false)
    private String activityName;

    @Column(name = "normal", nullable = false)
    private String normal;

    @Column(name = "recommendations", nullable = false)
    private String recommendations;

    @Column(name = "time", nullable = false)
    private Integer time;

    public Activity(Integer id, Patient patient, Date startTime, Date endTime, String activityName, String normal, String recommendations, Integer time) {
        this.id = id;
        this.patient = patient;
        this.startTime = startTime;
        this.endTime = endTime;
        this.activityName = activityName;
        this.normal = normal;
        this.recommendations = recommendations;
        this.time = time;
    }

    public Activity() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public String getNormal() {
        return normal;
    }

    public void setNormal(String normal) {
        this.normal = normal;
    }

    public String getRecommendations() {
        return recommendations;
    }

    public void setRecommendations(String recommendations) {
        this.recommendations = recommendations;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }
}
