package SD.services;

import SD.dto.MedicationDTO;
import SD.dto.builders.MedicationBuilder;
import SD.entities.Medication;
import SD.repositories.MedicationRepository;
import org.omg.PortableInterceptor.ORBInitInfoPackage.DuplicateName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MedicationService {

    @Autowired
    private final MedicationRepository medicationRepository;

    public MedicationService(MedicationRepository medicationRepository) {
        this.medicationRepository = medicationRepository;
    }

    public MedicationDTO findById(Integer id){
        Optional<Medication> medication = medicationRepository.findById(id);

        if(!medication.isPresent()){
            throw new ResourceNotFoundException();
        }
        return MedicationBuilder.generateDTOFromEntity(medication.get());
    }

    public MedicationDTO findByName(String name) {
        Medication medication = medicationRepository.findByName(name);

        return MedicationBuilder.generateDTOFromEntity(medication);
    }

    public List<MedicationDTO> findAll(){
        List<Medication> medications = medicationRepository.findAll();
        return medications.stream()
                .map(MedicationBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Integer insert(MedicationDTO medicationDTO) throws DuplicateName {
        Medication medication = medicationRepository.findByName(medicationDTO.getName());
        if(medication != null){
            throw new DuplicateName(medicationDTO.getName());
        }
        return medicationRepository.save(MedicationBuilder.generateEntityFromDTO(medicationDTO)).getId();
    }

    public Integer update(MedicationDTO medicationDTO){
        Optional<Medication> medication = medicationRepository.findById(medicationDTO.getId());
        if(!medication.isPresent()){
            throw new ResourceNotFoundException();
        }
        return medicationRepository.save(MedicationBuilder.generateEntityFromDTO(medicationDTO)).getId();
    }

    public void delete(Integer id){
        this.medicationRepository.deleteById(id);
    }
}
