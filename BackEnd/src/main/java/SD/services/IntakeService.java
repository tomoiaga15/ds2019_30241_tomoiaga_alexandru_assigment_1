package SD.services;

import SD.dto.IntakeDTO;
import SD.dto.builders.IntakeBuilder;
import SD.entities.Intake;
import SD.entities.enums.IntakeMoment;
import SD.repositories.IntakeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class IntakeService {

    @Autowired
    private final IntakeRepository intakeRepository;

    public IntakeService(IntakeRepository intakeRepository) {
        this.intakeRepository = intakeRepository;
    }

    public IntakeDTO findById(Integer id){
        Optional<Intake> intake = intakeRepository.findById(id);

        if(!intake.isPresent()){
            throw new ResourceNotFoundException();
        }
        return IntakeBuilder.generateDTOFromEntity(intake.get());
    }

    public IntakeDTO findByName(IntakeMoment name){
        Intake intake = intakeRepository.findByMoment(name);
        return IntakeBuilder.generateDTOFromEntity(intake);
    }
}
