package SD.services;

import SD.dto.RoleDTO;
import SD.dto.builders.RoleBuilder;
import SD.entities.Role;
import SD.repositories.RoleRepository;
import org.omg.PortableInterceptor.ORBInitInfoPackage.DuplicateName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class RoleService {

    @Autowired
    private final RoleRepository roleRepository;

    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    public RoleDTO findById(Integer id){
        Optional<Role> role = roleRepository.findById(id);

        if(!role.isPresent()){
            throw new ResourceNotFoundException();
        }

        return RoleBuilder.generateDTOFromEntity(role.get());
    }

    public List<RoleDTO> findAll(){
        List<Role> roles = roleRepository.findAll();
        return roles.stream()
                .map(RoleBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Integer insert(RoleDTO roleDTO) throws DuplicateName {
        Role role = roleRepository.findByType(roleDTO.getType());
        if(role != null){
            throw new DuplicateName(roleDTO.getType());
        }
        return roleRepository.save(RoleBuilder.generateEntityFromDTO(roleDTO)).getId();
    }

    public Integer update(RoleDTO roleDTO){
        Optional<Role> role = roleRepository.findById(roleDTO.getId());

        if(!role.isPresent()){
            throw new ResourceNotFoundException();
        }

        return roleRepository.save(RoleBuilder.generateEntityFromDTO(roleDTO)).getId();
    }

    public void delete(Integer id){
        roleRepository.deleteById(id);
    }
}
