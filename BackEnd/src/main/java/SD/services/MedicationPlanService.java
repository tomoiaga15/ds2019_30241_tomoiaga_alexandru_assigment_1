package SD.services;

import SD.dto.MedicationPlanDTO;
import SD.dto.builders.MedicationPlanBuilder;
import SD.entities.MedicationPlan;
import SD.repositories.MedicationPlanRepository;
import org.omg.PortableInterceptor.ORBInitInfoPackage.DuplicateName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MedicationPlanService {

    @Autowired
    private final MedicationPlanRepository medicationPlanRepository;

    public MedicationPlanService(MedicationPlanRepository medicationPlanRepository) {
        this.medicationPlanRepository = medicationPlanRepository;
    }

    public MedicationPlanDTO findById(Integer id){
        Optional<MedicationPlan> medicationPlan = medicationPlanRepository.findById(id);

        if(!medicationPlan.isPresent()){
            throw new ResourceNotFoundException();
        }
        return MedicationPlanBuilder.generateDTOFromEntity(medicationPlan.get());
    }

    public MedicationPlanDTO findByPatientId(Integer id){
        return MedicationPlanBuilder.generateDTOFromEntity(medicationPlanRepository.findByPatientId(id));
    }

    public List<MedicationPlanDTO> findAll(){
        List<MedicationPlan> medications = medicationPlanRepository.findAll();
        return medications.stream()
                .map(MedicationPlanBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Integer insert(MedicationPlanDTO medicationPlanDTO) throws DuplicateName {
        MedicationPlan medicationPlan = medicationPlanRepository.findByPatientId(medicationPlanDTO.getPatient().getId());
        if(medicationPlan != null){
            throw new DuplicateName(medicationPlanDTO.toString());
        }
        return medicationPlanRepository.save(MedicationPlanBuilder.generateEntityFromDTO(medicationPlanDTO)).getId();
    }

    public Integer update(MedicationPlanDTO medicationPlanDTO){
        Optional<MedicationPlan> medicationPlan = medicationPlanRepository.findById(medicationPlanDTO.getId());
        if(!medicationPlan.isPresent()){
            throw new ResourceNotFoundException();
        }
        return medicationPlanRepository.save(MedicationPlanBuilder.generateEntityFromDTO(medicationPlanDTO)).getId();
    }

    public void delete(Integer id){
        this.medicationPlanRepository.deleteById(id);
    }
}
