package SD.services;

import SD.dto.*;
import SD.dto.builders.IntakeBuilder;
import SD.dto.builders.MedicationBuilder;
import SD.dto.builders.PatientBuilder;
import SD.dto.builders.PillTakeBuilder;
import SD.entities.Intake;
import SD.entities.Patient;
import SD.entities.PillTake;
import SD.entities.enums.IntakeMoment;
import SD.grpc.MedicationOuterClass;
import SD.grpc.medicamentationGrpc;
import io.grpc.stub.StreamObserver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

import static java.lang.Integer.parseInt;

@Component
public class GrpcService extends medicamentationGrpc.medicamentationImplBase {
    @Autowired
    private final MedicationPlanService medicationPlanService;

    @Autowired
    private final DosageService dosageService;

    @Autowired
    private final MedicationPerPlanService medicationPerPlanService;

    @Autowired
    private final IntakeService intakeService;

    @Autowired
    private final MedicationService medicationService;

    @Autowired
    private final PillTakeService pillTakeService;

    @Autowired
    private final PatientService patientService;

    public GrpcService(MedicationPlanService medicationPlanService, DosageService dosageService, MedicationPerPlanService medicationPerPlanService, IntakeService intakeService, MedicationService medicationService, PillTakeService pillTakeService, PatientService patientService) {
        this.medicationPlanService = medicationPlanService;
        this.dosageService = dosageService;
        this.medicationPerPlanService = medicationPerPlanService;
        this.intakeService = intakeService;
        this.medicationService = medicationService;
        this.pillTakeService = pillTakeService;
        this.patientService = patientService;
    }

    @Override
    public void sendDate(MedicationOuterClass.Date date, StreamObserver<MedicationOuterClass.PillResponse> pillResponseStreamObserver) {
        System.out.println("Inside sendDate");

        String requestDateString = date.getDate();
        String[] requestDateList = requestDateString.split("-");
        Date requestDate = new Date(parseInt(requestDateList[0])-1900,parseInt(requestDateList[1])-1 ,parseInt(requestDateList[2]));
        MedicationOuterClass.PillResponse.Builder pillResponse = MedicationOuterClass.PillResponse.newBuilder();
        List<MedicationPlanDTO> medicationPlans = medicationPlanService.findAll();

        for (MedicationPlanDTO mp: medicationPlans) {
            System.out.println(mp.getId());
            Date start = new Date(mp.getStartTime().getYear(), mp.getStartTime().getMonth(), mp.getStartTime().getDate());
            Date end = new Date(mp.getEndTime().getYear(), mp.getEndTime().getMonth(), mp.getEndTime().getDate());
            if (start.before(requestDate) && end.after(requestDate)){
                MedicationOuterClass.Plan.Builder plan = MedicationOuterClass.Plan.newBuilder();
                plan.setPatient(mp.getPatient().getId());
                System.out.println(plan.getPatient());
                List<MedicationPerPlanDTO> medicationPerPlans = medicationPerPlanService.findByMedicationPlanId(mp.getId());

                for(MedicationPerPlanDTO mpp: medicationPerPlans){
                    System.out.println(mpp.getId());
                    List<DosageDTO> dosageDTOS = dosageService.getDosageForMPId(mpp.getId());
                    for(DosageDTO d: dosageDTOS){
                        MedicationOuterClass.Medication.Builder medication = MedicationOuterClass.Medication.newBuilder();
                        medication.setIntake(IntakeBuilder.generateEntityFromDTO(intakeService.findById(d.getIntake().getId())).getIntakeMoment().name());
                        medication.setName(MedicationBuilder.generateEntityFromDTO(medicationService.findById(mpp.getMedication().getId())).getName());
                        plan.addMedication(medication);
                    }
                }
                pillResponse.addPlan(plan);
            }
        }

        pillResponseStreamObserver.onNext(pillResponse.build());
        pillResponseStreamObserver.onCompleted();
    }

    public void taken(SD.grpc.MedicationOuterClass.Take request,
                      StreamObserver<MedicationOuterClass.Take> takeStreamObserver) {
        System.out.println(request);

        String[] strings = request.toString().split(" ");

        PillTake pillTake = new PillTake();

        MedicationDTO medication = medicationService.findByName(strings[2]);
        PatientDTO patient = patientService.findById(parseInt(strings[6]));
        IntakeDTO intake = intakeService.findByName(IntakeMoment.valueOf(strings[9]));
        Date date = new Date(parseInt(strings[16].split("\"")[0].split("-")[0])-1900,parseInt(strings[16].split("\"")[0].split("-")[1])-1 ,parseInt(strings[16].split("\"")[0].split("-")[2]));

        pillTake.setIntake(IntakeBuilder.generateEntityFromDTO(intake));
        pillTake.setPatient(PatientBuilder.generateEntityFromDTO(patient));
        pillTake.setMedication(MedicationBuilder.generateEntityFromDTO(medication));
        pillTake.setTaken("yes");
        pillTake.setDate(date);

        pillTakeService.insert(PillTakeBuilder.generateDTOFromEntity(pillTake));

        MedicationOuterClass.Take.Builder take = MedicationOuterClass.Take.newBuilder();
        take.setMsg(request.getMsg());
        takeStreamObserver.onNext(take.build());
        takeStreamObserver.onCompleted();
    }

    public void notTaken(SD.grpc.MedicationOuterClass.NotTaken request,
                         StreamObserver<MedicationOuterClass.NotTaken> notTakeStreamObserver) {
        for(String s:request.getMsgList()) {
            System.out.println(s);


            String[] strings = s.split(" ");

            PillTake pillTake = new PillTake();

            System.out.println(strings[6]);
            System.out.println(strings[9]);
            System.out.println(strings[12]);
            System.out.println(strings[16]);

            MedicationDTO medication = medicationService.findByName(strings[6]);
            PatientDTO patient = patientService.findById(parseInt(strings[9]));
            IntakeDTO intake = intakeService.findByName(IntakeMoment.valueOf(strings[12]));
            Date date = new Date(parseInt(strings[16].split("\"")[0].split("-")[0])-1900,parseInt(strings[16].split("\"")[0].split("-")[1])-1 ,parseInt(strings[16].split("\"")[0].split("-")[2]));

            pillTake.setIntake(IntakeBuilder.generateEntityFromDTO(intake));
            pillTake.setPatient(PatientBuilder.generateEntityFromDTO(patient));
            pillTake.setMedication(MedicationBuilder.generateEntityFromDTO(medication));
            pillTake.setTaken("no");
            pillTake.setDate(date);

            pillTakeService.insert(PillTakeBuilder.generateDTOFromEntity(pillTake));

        }

        MedicationOuterClass.NotTaken.Builder notTake = MedicationOuterClass.NotTaken.newBuilder();
        notTakeStreamObserver.onNext(notTake.build());
        notTakeStreamObserver.onCompleted();
    }
}
