package SD.services;

import SD.dto.UserDTO;
import SD.dto.UserViewDTO;
import SD.dto.builders.UserBuilder;
import SD.dto.builders.UserViewBuilder;
import SD.entities.User;
import SD.repositories.UserRepository;
import org.omg.PortableInterceptor.ORBInitInfoPackage.DuplicateName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserService {

    @Autowired
    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public UserDTO findById(Integer id){
        Optional<User> user = userRepository.findById(id);

        if(!user.isPresent()){
            throw new ResourceNotFoundException();
        }
        return UserBuilder.generateDTOFromEntity(user.get());
    }

    public UserDTO findByUsername(String username){
        return UserBuilder.generateDTOFromEntity(userRepository.findByUserName(username));
    }

    public List<UserViewDTO> findAll(){
        List<User> users = userRepository.findAll();
        return users.stream()
                .map(UserViewBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Integer insert(UserDTO userDTO) throws DuplicateName {
        User user = userRepository.findByUserName(userDTO.getUserName());
        if(user != null){
            throw new DuplicateName(userDTO.getUserName());
        }
        return userRepository.save(UserBuilder.generateEntityFromDTO(userDTO)).getId();
    }

    public Integer update(UserDTO userDTO){
        Optional<User> user = userRepository.findById(userDTO.getId());
        if(!user.isPresent()){
            throw new ResourceNotFoundException();
        }
        return userRepository.save(UserBuilder.generateEntityFromDTO(userDTO)).getId();
    }

    public void delete(Integer id){
        this.userRepository.deleteById(id);
    }
}
