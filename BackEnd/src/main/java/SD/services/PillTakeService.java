package SD.services;

import SD.dto.PillTakeDTO;
import SD.dto.builders.PillTakeBuilder;
import SD.repositories.PillRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PillTakeService {

    @Autowired
    private final PillRepository pillRepository;

    public PillTakeService(PillRepository pillRepository) {
        this.pillRepository = pillRepository;
    }

    public Integer insert(PillTakeDTO pillTakeDTO){
        return pillRepository.save(PillTakeBuilder.generateEntityFromDTO(pillTakeDTO)).getId();
    }
}
