package SD.services;

import SD.dto.ActivityDTO;
import SD.dto.builders.ActivityBuilder;
import SD.entities.Activity;
import SD.repositories.ActivityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ActivityService {

    @Autowired
    private final ActivityRepository activityRepository;

    public ActivityService(ActivityRepository activityRepository) {
        this.activityRepository = activityRepository;
    }

    public Integer insert(ActivityDTO activityDTO) {
        return activityRepository.save(ActivityBuilder.generateEntityFromDTO(activityDTO)).getId();
    }

    public ActivityDTO findById(Integer id){
        Optional<Activity> dosage = activityRepository.findById(id);

        if(!dosage.isPresent()){
            throw new ResourceNotFoundException();
        }
        return ActivityBuilder.generateDTOFromEntity(dosage.get());
    }

    public List<ActivityDTO> findAll(){
        List<Activity> medications = activityRepository.findAll();
        return medications.stream()
                .map(ActivityBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }
}
