package SD.services;

import SD.dto.PatientDTO;
import SD.dto.builders.PatientBuilder;
import SD.entities.Patient;
import SD.repositories.PatientRepository;
import org.omg.PortableInterceptor.ORBInitInfoPackage.DuplicateName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PatientService {

    @Autowired
    private final PatientRepository patientRepository;

    public PatientService(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    public PatientDTO findById(Integer id){
        Optional<Patient> patient = patientRepository.findById(id);

        if(!patient.isPresent()){
            throw new ResourceNotFoundException();
        }
        return PatientBuilder.generateDTOFromEntity(patient.get());
    }

    public List<PatientDTO> findAll(){
        List<Patient> patients = patientRepository.findAll();
        return patients.stream()
                .map(PatientBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public List<PatientDTO> findAllPatietsForCaregiver(Integer id){
        List<Patient> patients = patientRepository.findAllPatietsForCaregiver(id);
        return patients.stream()
                .map(PatientBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public PatientDTO findByUserId(Integer id){
        return PatientBuilder.generateDTOFromEntity(patientRepository.findByUserId(id));
    }

    public List<PatientDTO> findAllPatietsForDoctor(Integer id){
        List<Patient> patients = patientRepository.findAllPatietsForDoctor(id);
        return patients.stream()
                .map(PatientBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Integer insert(PatientDTO patientDTO) throws DuplicateName {
        Patient patient = patientRepository.findByUserId(patientDTO.getUser().getId());
        if(patient != null){
            throw new DuplicateName(patientDTO.toString());
        }
        return patientRepository.save(PatientBuilder.generateEntityFromDTO(patientDTO)).getId();
    }

    public Integer update(PatientDTO patientDTO){
        Optional<Patient> patient = patientRepository.findById(patientDTO.getId());
        if(!patient.isPresent()){
            throw new ResourceNotFoundException();
        }
        return patientRepository.save(PatientBuilder.generateEntityFromDTO(patientDTO)).getId();
    }

    public void delete(Integer id){
        this.patientRepository.deleteById(id);
    }
}
