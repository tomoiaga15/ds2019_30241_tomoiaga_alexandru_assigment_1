package SD.grpc;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.15.0)",
    comments = "Source: medication.proto")
public final class medicamentationGrpc {

  private medicamentationGrpc() {}

  public static final String SERVICE_NAME = "medicamentation";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<SD.grpc.MedicationOuterClass.Date,
      SD.grpc.MedicationOuterClass.PillResponse> getSendDateMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "sendDate",
      requestType = SD.grpc.MedicationOuterClass.Date.class,
      responseType = SD.grpc.MedicationOuterClass.PillResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<SD.grpc.MedicationOuterClass.Date,
      SD.grpc.MedicationOuterClass.PillResponse> getSendDateMethod() {
    io.grpc.MethodDescriptor<SD.grpc.MedicationOuterClass.Date, SD.grpc.MedicationOuterClass.PillResponse> getSendDateMethod;
    if ((getSendDateMethod = medicamentationGrpc.getSendDateMethod) == null) {
      synchronized (medicamentationGrpc.class) {
        if ((getSendDateMethod = medicamentationGrpc.getSendDateMethod) == null) {
          medicamentationGrpc.getSendDateMethod = getSendDateMethod = 
              io.grpc.MethodDescriptor.<SD.grpc.MedicationOuterClass.Date, SD.grpc.MedicationOuterClass.PillResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "medicamentation", "sendDate"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  SD.grpc.MedicationOuterClass.Date.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  SD.grpc.MedicationOuterClass.PillResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new medicamentationMethodDescriptorSupplier("sendDate"))
                  .build();
          }
        }
     }
     return getSendDateMethod;
  }

  private static volatile io.grpc.MethodDescriptor<SD.grpc.MedicationOuterClass.Take,
      SD.grpc.MedicationOuterClass.Take> getTakenMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "taken",
      requestType = SD.grpc.MedicationOuterClass.Take.class,
      responseType = SD.grpc.MedicationOuterClass.Take.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<SD.grpc.MedicationOuterClass.Take,
      SD.grpc.MedicationOuterClass.Take> getTakenMethod() {
    io.grpc.MethodDescriptor<SD.grpc.MedicationOuterClass.Take, SD.grpc.MedicationOuterClass.Take> getTakenMethod;
    if ((getTakenMethod = medicamentationGrpc.getTakenMethod) == null) {
      synchronized (medicamentationGrpc.class) {
        if ((getTakenMethod = medicamentationGrpc.getTakenMethod) == null) {
          medicamentationGrpc.getTakenMethod = getTakenMethod = 
              io.grpc.MethodDescriptor.<SD.grpc.MedicationOuterClass.Take, SD.grpc.MedicationOuterClass.Take>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "medicamentation", "taken"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  SD.grpc.MedicationOuterClass.Take.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  SD.grpc.MedicationOuterClass.Take.getDefaultInstance()))
                  .setSchemaDescriptor(new medicamentationMethodDescriptorSupplier("taken"))
                  .build();
          }
        }
     }
     return getTakenMethod;
  }

  private static volatile io.grpc.MethodDescriptor<SD.grpc.MedicationOuterClass.NotTaken,
      SD.grpc.MedicationOuterClass.NotTaken> getNotTakenMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "notTaken",
      requestType = SD.grpc.MedicationOuterClass.NotTaken.class,
      responseType = SD.grpc.MedicationOuterClass.NotTaken.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<SD.grpc.MedicationOuterClass.NotTaken,
      SD.grpc.MedicationOuterClass.NotTaken> getNotTakenMethod() {
    io.grpc.MethodDescriptor<SD.grpc.MedicationOuterClass.NotTaken, SD.grpc.MedicationOuterClass.NotTaken> getNotTakenMethod;
    if ((getNotTakenMethod = medicamentationGrpc.getNotTakenMethod) == null) {
      synchronized (medicamentationGrpc.class) {
        if ((getNotTakenMethod = medicamentationGrpc.getNotTakenMethod) == null) {
          medicamentationGrpc.getNotTakenMethod = getNotTakenMethod = 
              io.grpc.MethodDescriptor.<SD.grpc.MedicationOuterClass.NotTaken, SD.grpc.MedicationOuterClass.NotTaken>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "medicamentation", "notTaken"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  SD.grpc.MedicationOuterClass.NotTaken.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  SD.grpc.MedicationOuterClass.NotTaken.getDefaultInstance()))
                  .setSchemaDescriptor(new medicamentationMethodDescriptorSupplier("notTaken"))
                  .build();
          }
        }
     }
     return getNotTakenMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static medicamentationStub newStub(io.grpc.Channel channel) {
    return new medicamentationStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static medicamentationBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new medicamentationBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static medicamentationFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new medicamentationFutureStub(channel);
  }

  /**
   */
  public static abstract class medicamentationImplBase implements io.grpc.BindableService {

    /**
     */
    public void sendDate(SD.grpc.MedicationOuterClass.Date request,
        io.grpc.stub.StreamObserver<SD.grpc.MedicationOuterClass.PillResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getSendDateMethod(), responseObserver);
    }

    /**
     */
    public void taken(SD.grpc.MedicationOuterClass.Take request,
        io.grpc.stub.StreamObserver<SD.grpc.MedicationOuterClass.Take> responseObserver) {
      asyncUnimplementedUnaryCall(getTakenMethod(), responseObserver);
    }

    /**
     */
    public void notTaken(SD.grpc.MedicationOuterClass.NotTaken request,
        io.grpc.stub.StreamObserver<SD.grpc.MedicationOuterClass.NotTaken> responseObserver) {
      asyncUnimplementedUnaryCall(getNotTakenMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getSendDateMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                SD.grpc.MedicationOuterClass.Date,
                SD.grpc.MedicationOuterClass.PillResponse>(
                  this, METHODID_SEND_DATE)))
          .addMethod(
            getTakenMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                SD.grpc.MedicationOuterClass.Take,
                SD.grpc.MedicationOuterClass.Take>(
                  this, METHODID_TAKEN)))
          .addMethod(
            getNotTakenMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                SD.grpc.MedicationOuterClass.NotTaken,
                SD.grpc.MedicationOuterClass.NotTaken>(
                  this, METHODID_NOT_TAKEN)))
          .build();
    }
  }

  /**
   */
  public static final class medicamentationStub extends io.grpc.stub.AbstractStub<medicamentationStub> {
    private medicamentationStub(io.grpc.Channel channel) {
      super(channel);
    }

    private medicamentationStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected medicamentationStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new medicamentationStub(channel, callOptions);
    }

    /**
     */
    public void sendDate(SD.grpc.MedicationOuterClass.Date request,
        io.grpc.stub.StreamObserver<SD.grpc.MedicationOuterClass.PillResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getSendDateMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void taken(SD.grpc.MedicationOuterClass.Take request,
        io.grpc.stub.StreamObserver<SD.grpc.MedicationOuterClass.Take> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getTakenMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void notTaken(SD.grpc.MedicationOuterClass.NotTaken request,
        io.grpc.stub.StreamObserver<SD.grpc.MedicationOuterClass.NotTaken> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getNotTakenMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class medicamentationBlockingStub extends io.grpc.stub.AbstractStub<medicamentationBlockingStub> {
    private medicamentationBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private medicamentationBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected medicamentationBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new medicamentationBlockingStub(channel, callOptions);
    }

    /**
     */
    public SD.grpc.MedicationOuterClass.PillResponse sendDate(SD.grpc.MedicationOuterClass.Date request) {
      return blockingUnaryCall(
          getChannel(), getSendDateMethod(), getCallOptions(), request);
    }

    /**
     */
    public SD.grpc.MedicationOuterClass.Take taken(SD.grpc.MedicationOuterClass.Take request) {
      return blockingUnaryCall(
          getChannel(), getTakenMethod(), getCallOptions(), request);
    }

    /**
     */
    public SD.grpc.MedicationOuterClass.NotTaken notTaken(SD.grpc.MedicationOuterClass.NotTaken request) {
      return blockingUnaryCall(
          getChannel(), getNotTakenMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class medicamentationFutureStub extends io.grpc.stub.AbstractStub<medicamentationFutureStub> {
    private medicamentationFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private medicamentationFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected medicamentationFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new medicamentationFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<SD.grpc.MedicationOuterClass.PillResponse> sendDate(
        SD.grpc.MedicationOuterClass.Date request) {
      return futureUnaryCall(
          getChannel().newCall(getSendDateMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<SD.grpc.MedicationOuterClass.Take> taken(
        SD.grpc.MedicationOuterClass.Take request) {
      return futureUnaryCall(
          getChannel().newCall(getTakenMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<SD.grpc.MedicationOuterClass.NotTaken> notTaken(
        SD.grpc.MedicationOuterClass.NotTaken request) {
      return futureUnaryCall(
          getChannel().newCall(getNotTakenMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_SEND_DATE = 0;
  private static final int METHODID_TAKEN = 1;
  private static final int METHODID_NOT_TAKEN = 2;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final medicamentationImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(medicamentationImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_SEND_DATE:
          serviceImpl.sendDate((SD.grpc.MedicationOuterClass.Date) request,
              (io.grpc.stub.StreamObserver<SD.grpc.MedicationOuterClass.PillResponse>) responseObserver);
          break;
        case METHODID_TAKEN:
          serviceImpl.taken((SD.grpc.MedicationOuterClass.Take) request,
              (io.grpc.stub.StreamObserver<SD.grpc.MedicationOuterClass.Take>) responseObserver);
          break;
        case METHODID_NOT_TAKEN:
          serviceImpl.notTaken((SD.grpc.MedicationOuterClass.NotTaken) request,
              (io.grpc.stub.StreamObserver<SD.grpc.MedicationOuterClass.NotTaken>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class medicamentationBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    medicamentationBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return SD.grpc.MedicationOuterClass.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("medicamentation");
    }
  }

  private static final class medicamentationFileDescriptorSupplier
      extends medicamentationBaseDescriptorSupplier {
    medicamentationFileDescriptorSupplier() {}
  }

  private static final class medicamentationMethodDescriptorSupplier
      extends medicamentationBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    medicamentationMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (medicamentationGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new medicamentationFileDescriptorSupplier())
              .addMethod(getSendDateMethod())
              .addMethod(getTakenMethod())
              .addMethod(getNotTakenMethod())
              .build();
        }
      }
    }
    return result;
  }
}
