//package SD;
//
//import SD.services.DosageService;
//import SD.services.GrpcService;
//import SD.services.MedicationPerPlanService;
//import SD.services.MedicationPlanService;
//import io.grpc.BindableService;
//import io.grpc.Server;
//import io.grpc.ServerBuilder;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.CommandLineRunner;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.stereotype.Component;
//
//import java.io.IOException;
//
//@Component
//public class GrpcServer implements CommandLineRunner {
//
//    @Autowired
//    private GrpcService grpcService;
//
//    public GrpcServer(GrpcService grpcService) {
//        this.grpcService = grpcService;
//    }
//
//    @Override
//    public void run(String... args) throws IOException, InterruptedException {
//        System.out.println("starting GRPC Server");
//        Server server;
//        server = ServerBuilder.forPort(9091).addService(grpcService).build();
//        try{
//            server.start();
//        }
//        catch (IOException e){
//            e.printStackTrace();
//        }
//        System.out.println("server started at "+ server.getPort());
//
//        try{
//            server.awaitTermination();
//        }
//        catch (InterruptedException e){
//            e.printStackTrace();
//        }
//    }
//}
