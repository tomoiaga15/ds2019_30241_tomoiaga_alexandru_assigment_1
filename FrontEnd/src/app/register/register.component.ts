import { Component, OnInit } from '@angular/core';
import {UserService} from '../services/user.service';
import {User} from '../model/User';
import {PatientService} from '../services/patient.service';
import {Patient} from '../model/Patient';
import {Router} from '@angular/router';
import {RoleService} from '../services/roleService';
import {Role} from '../model/Role';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private userService: UserService,
              private patientService: PatientService,
              private roleService: RoleService,
              private router: Router) { }

  user: User;
  doctor: User;
  caregiver: User;
  role: Role;

  ngOnInit() {
    this.getDoctor();
    this.getCaregiver();
    this.getRole();
  }

  getDoctor() {
    this.userService.getUserById(1)
      .subscribe(data => {
        this.doctor = data;
      });
  }

  getRole() {
    this.roleService.getRoleById(3)
      .subscribe( data => {
        this.role = data;
      });
  }

  getCaregiver() {
    this.userService.getUserById(2)
      .subscribe(data => {
        this.caregiver = data;
      });
  }

  save(name, birthdate, gender, address, userName, password) {
    if (!name || !birthdate || !address || !userName || !password) {
      alert('Date incomplete');
    } else {
      const newUser = new User();
      newUser.name = name;
      newUser.birthdate = birthdate;
      newUser.address = address;
      newUser.role = this.role;
      newUser.userName = userName;
      newUser.password = password;
      newUser.gender = gender;
      this.userService.insertUser(newUser)
        .subscribe(data => {
          console.log(data);
          this.userService.getUserById(data)
            .subscribe(data1 => {
              const patient = new Patient();
              patient.user = data1;
              patient.caregiver = this.caregiver;
              patient.doctor = this.doctor;
              patient.medicalRecord = '';
              console.log(patient);
              this.patientService.insertPatient(patient)
              // tslint:disable-next-line:no-shadowed-variable
                .subscribe( data2 => {
                  this.router.navigate(['']);
                });
            });

      });
    }
  }

}
