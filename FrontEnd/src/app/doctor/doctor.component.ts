import { Component, OnInit } from '@angular/core';
import {UserService} from '../services/user.service';
import {UserView} from '../model/UserView';
import {ActivatedRoute, Router} from '@angular/router';
import {Medication} from '../model/Medication';
import {MedicationPlan} from '../model/MedicationPlan';
import {MedicationPlanService} from '../services/medicationPlan.service';
import {MedicationService} from '../services/medication.service';
import {Patient} from '../model/Patient';
import {PatientService} from '../services/patient.service';

@Component({
  selector: 'app-doctor',
  templateUrl: './doctor.component.html',
  styleUrls: ['./doctor.component.css']
})
export class DoctorComponent implements OnInit {

  constructor(private userService: UserService,
              private medicationService: MedicationService,
              private patientService: PatientService,
              private medicationPlanService: MedicationPlanService,
              private route: ActivatedRoute,
              private router: Router) { }

  users: UserView[] = [];
  patients: Patient[];
  patient: Patient;
  medications: Medication[] = [];
  medicationPlans: MedicationPlan[] =[];

  ngOnInit() {
    this.getUsers();
    this.getMedications();
    this.getMedicationPlans();
    this.getPatients();
  }

  getUsers() {
    this.userService.getUsers()
      .subscribe(users => {
        this.users = users;
      });
  }

  getPatients() {
    this.patientService.getPatient().subscribe(data => {
      this.patients = data ;
    });
  }

  getMedications() {
    this.medicationService.getMedication()
      .subscribe(medications => {
        this.medications = medications;
      });
  }

  getMedicationPlans() {
    this.medicationPlanService.getMedicationPlan()
      .subscribe(medicationPlans => {
        this.medicationPlans = medicationPlans;
      });
  }

  delete(id) {
    this.medicationPlanService.deleteMedicationPlan(id).subscribe(data => {
      console.log('id');
      this.getMedicationPlans();
  });
  }

  addMedication(name, sideEfect) {
    const med = new Medication();
    med.sideEfect = sideEfect;
    med.name = name;
    console.log(med);
    console.log(sideEfect);
    console.log(name);
    this.medicationService.insertMedication(med)
      .subscribe(data => {
        this.getMedications();
      });
  }

  addMedicationPlan(startTime, endTime, patientId, medicalRecord) {
    const medPlan = new MedicationPlan();
    medPlan.endTime = endTime;
    medPlan.startTime = startTime;
    console.log(medPlan);
    this.patientService.getPatientById(patientId)
      .subscribe( data => {
        console.log(data);
        medPlan.patient = data;
        console.log(medPlan);
        this.medicationPlanService.insertMedicationPlan(medPlan)
          .subscribe( data1 => {
            // @ts-ignore
            medPlan.patient.medicalRecord = medicalRecord;
            this.patientService.updatePatient(medPlan.patient as Patient).subscribe(dataaa => {
              this.getMedicationPlans();
            });
        });
      });
  }

  goEditUser(id) {
    this.router.navigate(['user/' + id]);
  }

  goEditMedication(id) {
    this.router.navigate(['medication/' + id]);
  }

  goEditMedicationPlan(id) {
    this.router.navigate(['medicationplan/' + id]);
  }

  goEditPatient(id: number) {
    this.router.navigate(['patientDetail/' + id]);
  }
}
