import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {PatientService} from '../services/patient.service';
import {UserService} from '../services/user.service';
import {MedicationPlanService} from '../services/medicationPlan.service';
import {MedicationPerPlanService} from '../services/medicationPerPlan.service';
import {DosageService} from '../services/dosage.service';
import {Patient} from '../model/Patient';
import {User} from '../model/User';
import {ActivityService} from '../services/activity.service';
import {map} from 'rxjs/operators';
import {Pill} from '../model/Pill';
import {Activity} from '../model/Activity';

@Component({
  selector: 'app-patient',
  templateUrl: './patient.component.html',
  styleUrls: ['./patient.component.css']
})
export class PatientComponent implements OnInit {


  adsadad;
  pills: Pill[] = [];

  actions: Activity[] = [];

  view: any[] = [600, 400];
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Action';
  showYAxisLabel = true;
  yAxisLabel = 'Hours';
  timeline = true;

  colorScheme = {
    domain: ['#9370DB', '#87CEFA', '#FA8072', '#FF7F50', '#90EE90', '#9370DB', '#87CEFA', '#90EE90', '#9370DB', '#FA8072']
  };
  showLabels = true;
  public single;


  constructor(private route: ActivatedRoute,
              private patientService: PatientService,
              private userService: UserService,
              private medicationPlanService: MedicationPlanService,
              private medicationPerPlanService: MedicationPerPlanService,
              private dosageService: DosageService,
              private router: Router,
              private activityService: ActivityService) {
  }

  patient: Patient;
  doctor: User;
  user: User;
  caregiver: User;

  ngOnInit() {
    this.getPatient();
    this.getData('2020-01-08');
  }

  getPatient() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.patientService.getPatientById(id)
      .subscribe(user => {
        this.patient = user;
        // @ts-ignore
        this.user = user.user;
        // @ts-ignore
        this.doctor = user.doctor;
        // @ts-ignore
        this.caregiver = user.caregiver;
      });
  }


  update(doctorId, caregiverId) {
    this.userService.getUserById(doctorId)
      .subscribe(data => {
        this.doctor = data;
        this.userService.getUserById(caregiverId)
          .subscribe(data1 => {
            this.caregiver = data1;
            this.patient.doctor = this.doctor;
            this.patient.caregiver = this.caregiver;
            this.patientService.updatePatient(this.patient).subscribe(data2 => {
              this.getPatient();
            });
          });
      });
  }

  goBack() {
    this.router.navigate(['doctor']);
  }

  private getGraph(s: string) {
    this.activityService.getGraph(s)
      .subscribe(graph => {
        this.adsadad = graph;
        // console.log(this.adsadad.activity);
        const newVar = new Array();
        newVar.push({name: 'Breakfast', value: this.adsadad.activity.Breakfast / 3600000});
        newVar.push({name: 'Grooming', value: this.adsadad.activity.Grooming / 3600000});
        newVar.push({name: 'Dinner', value: this.adsadad.activity.Dinner / 3600000});
        newVar.push({name: 'Leaving', value: this.adsadad.activity.Leaving / 3600000});
        newVar.push({name: 'Lunch', value: this.adsadad.activity.Lunch / 3600000});
        newVar.push({name: 'Showering', value: this.adsadad.activity.Showering / 3600000});
        newVar.push({name: 'Sleeping', value: this.adsadad.activity.Sleeping / 3600000});
        newVar.push({name: 'Snack', value: this.adsadad.activity.Snack / 3600000});
        newVar.push({name: 'Toileting', value: this.adsadad.activity.Toileting / 3600000});
        newVar.push({name: 'Spare_Time/TV', value: this.adsadad.activity['Spare_Time/TV'] / 3600000});
        this.single = newVar;
      });
  }

  private getPills(s: string) {
    this.activityService.getPills(s)
      .subscribe(
        pills => {
          this.pills = pills.activity;
        });
  }

  getData(value: string) {
    const id = +this.route.snapshot.paramMap.get('id');
    this.getGraph(value + '-' + id);
    this.getPills(value + '-' + id);
    this.getActivities('' + id);
  }

  private getActivities(value: string) {
    this.activityService.getActivity(value)
      .subscribe(activities => {
        this.actions = activities.activity;
        console.log(this.actions);
      });
  }

  updateNormal(value: string, action: number) {
    const id = +this.route.snapshot.paramMap.get('id');
    this.activityService.updateNormal(value, action)
      .subscribe(nn => {
        console.log(action);
        this.getActivities('' + id);
      });
  }

  updateRecommendation(value: string, action: number) {
    const id = +this.route.snapshot.paramMap.get('id');
    this.activityService.updateRecommendation(value, action)
      .subscribe(nn => {
        console.log(action);
        this.getActivities('' + id);
      });
  }
}
