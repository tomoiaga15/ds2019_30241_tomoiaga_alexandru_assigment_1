import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule} from '@angular/router';

import { AppComponent } from './app.component';
import { UserComponent } from './user/user.component';
import { HomeComponent } from './home/home.component';
import { DoctorComponent } from './doctor/doctor.component';
import { PatientComponent } from './patient/patient.component';
import {HttpClientModule} from '@angular/common/http';
import { RegisterComponent } from './register/register.component';
import { PatientDetailComponent } from './patient-detail/patient-detail.component';
import { CaregiverDetailComponent } from './caregiver-detail/caregiver-detail.component';
import {FormsModule} from '@angular/forms';
import { MedicationComponent } from './medication/medication.component';
import { MedicationPlanComponent } from './medication-plan/medication-plan.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import { TestComponent } from './test/test.component';

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    HomeComponent,
    DoctorComponent,
    PatientComponent,
    RegisterComponent,
    PatientDetailComponent,
    CaregiverDetailComponent,
    MedicationComponent,
    MedicationPlanComponent,
    TestComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgxChartsModule,
    BrowserAnimationsModule,
    RouterModule.forRoot([{
      path: '',
      component: HomeComponent
    },
      {
        path: 'register',
        component: RegisterComponent
      },
      {
        path: 'doctor',
        component: DoctorComponent
      },
      {
        path: 'patient/:id',
        component: PatientDetailComponent
      },
      {
        path: 'patientDetail/:id',
        component: PatientComponent
      },
      {
        path: 'user/:id',
        component: UserComponent
      },
      {
        path: 'caregiver/:id',
        component: CaregiverDetailComponent
      },
      {
        path: 'medication/:id',
        component: MedicationComponent
      },
      {
        path: 'medicationplan/:id',
        component: MedicationPlanComponent
      },
      {
        path: 'test',
        component: TestComponent
      }
    ]),
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
