import { Component, OnInit } from '@angular/core';
import {Patient} from '../model/Patient';
import {ActivatedRoute, Router} from '@angular/router';
import {PatientService} from '../services/patient.service';
import {UserService} from '../services/user.service';
import {User} from '../model/User';
import {MedicationPlanService} from '../services/medicationPlan.service';
import {MedicationPlan} from '../model/MedicationPlan';
import {MedicationPerPlan} from '../model/MedicationPerPlan';
import {MedicationPerPlanService} from '../services/medicationPerPlan.service';
import {DosageService} from '../services/dosage.service';

@Component({
  selector: 'app-patient-detail',
  templateUrl: './patient-detail.component.html',
  styleUrls: ['./patient-detail.component.css']
})
export class PatientDetailComponent implements OnInit {

  patient: Patient;
  user: User;
  medicationPlan: MedicationPlan;
  medicationPerPlan: MedicationPerPlan[];
  // @ts-ignore
  map = new Map();
  // tslint:disable-next-line:max-line-length
  constructor(private route: ActivatedRoute,
              private patientService: PatientService,
              private userService: UserService,
              private medicationPlanService: MedicationPlanService,
              private medicationPerPlanService: MedicationPerPlanService,
              private dosageService: DosageService) { }

  ngOnInit() {
    this.getPatient();
  }

  getPatient() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.patientService.getPatientById(id).subscribe(data => {
      this.patient = data;
      this.viewMedicationPlan();
      // @ts-ignore
      this.userService.getUserById(data.user.id).subscribe(data1 => {
        this.user = data1;
      });
    });
  }

  viewMedicationPlan() {
    this.medicationPlanService.getMedicationPlanForPatient(this.patient.id)
      .subscribe(data => {
        this.medicationPlan = data;
        // @ts-ignore
        this.medicationPerPlanService.getMedicationPerPlanByMedicationPlanId(data.id)
          .subscribe(data1 => {
            // @ts-ignore
            this.medicationPerPlan = data1;
            // tslint:disable-next-line:prefer-for-of
            for (let i = 0 ; i < this.medicationPerPlan.length; i++) {
              this.dosageService.getDosageByMedicationPerPlanId(this.medicationPerPlan[i].id)
              .subscribe(data2 => {
                // @ts-ignore
                this.map.set(this.medicationPerPlan[i].id, data2);
              });
            }
            });
      });
  }

  getKey(id: number) {
    return this.map.get(id);
  }

  save() {
    this.userService.updateUser(this.user).subscribe(data => {
      alert('User updated');
      this.getPatient();
    });
  }
}
