import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from '../services/user.service';
import {User} from '../model/User';
import {PatientService} from '../services/patient.service';
import {Patient} from '../model/Patient';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private router: Router, private userService: UserService, private patientService: PatientService) { }

  user: User = null;
  patient: Patient = null;

  ngOnInit() {
  }

  goStart(username, password) {
    this.userService.getUserByUsername(username)
      .subscribe(data => {
        this.user = data;
        console.log(this.userService);
        if (this.user) {
          if (data.password !== password || data.role === null) {
            return false;
          }
          // @ts-ignore
          if ('Doctor' === data.role.type) {
            this.router.navigate(['doctor']);
          }

          // @ts-ignore
          if ('Pacient' === data.role.type) {
            this.patientService.getPatientByUserId(this.user.id).subscribe(data1 => {
              this.patient = data1;
              if (data1) {
                this.router.navigate(['patient/' + data1.id]);
              }
            });
            console.log(this.patient);
            // @ts-ignore
          }

          // @ts-ignore
          if ('Caregiver' === this.user.role.type) {
            this.router.navigate(['caregiver/' + this.user.id]);
          }
        }
      });

    console.log(this.user);
    return false;
  }

  goSignIn() {
    this.router.navigate(['register']);
  }

}
