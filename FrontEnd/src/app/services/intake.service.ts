import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Intake} from '../model/Intake';
import {REST_API} from '../common/API';
import {Observable} from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class IntakeService {

  constructor(private http: HttpClient) {
  }
  getIntakeById(id: number): Observable<Intake> {
    return this.http.get<Intake>(REST_API + 'intake/' + id);
  }

}
