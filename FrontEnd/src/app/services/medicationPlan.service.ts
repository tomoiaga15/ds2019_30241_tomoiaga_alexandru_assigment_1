import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MedicationPlan} from '../model/MedicationPlan';
import {REST_API} from '../common/API';
import {Observable} from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class MedicationPlanService {

  constructor(private http: HttpClient) {
  }

  getMedicationPlan(): Observable<MedicationPlan[]> {
    return this.http.get<MedicationPlan[]>(REST_API + 'medicationplan');
  }

  insertMedicationPlan(medicationPlan: MedicationPlan): Observable<MedicationPlan> {
    return this.http.post<any>(REST_API + 'medicationplan', medicationPlan);
  }

  updateMedicationPlan(medicationPlan: MedicationPlan): Observable<any> {
    return this.http.put<any>(REST_API + 'medicationplan', medicationPlan);
  }

  deleteMedicationPlan(id: number): Observable<any> {
    return this.http.delete<any>(REST_API + 'medicationplan/' + id);
  }

  getMedicationPlanById(id: number): Observable<MedicationPlan> {
    return this.http.get<MedicationPlan>(REST_API + 'medicationplan/' + id);
  }

  getMedicationPlanForPatient(id: number): Observable<MedicationPlan> {
    return this.http.get<any>(REST_API + 'medicationplan/patient/' + id);
  }
}
