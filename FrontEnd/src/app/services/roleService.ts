import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Role} from '../model/Role';
import {REST_API} from '../common/API';
import {Observable} from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class RoleService {

  constructor(private http: HttpClient) {
  }
  getRoleById(id: number): Observable<Role> {
    return this.http.get<Role>(REST_API + 'role/' + id);
  }

}
