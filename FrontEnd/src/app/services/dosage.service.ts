import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Dosage} from '../model/Dosage';
import {REST_API} from '../common/API';
import {Observable} from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class DosageService {

  constructor(private http: HttpClient) {
  }

  getDosage(): Observable<Dosage[]> {
    return this.http.get<Dosage[]>(REST_API + 'dosage');
  }

  insertDosage(dosage: Dosage): Observable<Dosage> {
    return this.http.post<any>(REST_API + 'dosage', dosage);
  }

  updateDosage(dosage: Dosage): Observable<Dosage> {
    return this.http.put<any>(REST_API + 'dosage', dosage);
  }

  getDosageById(id: number): Observable<Dosage> {
    return this.http.get<Dosage>(REST_API + 'dosage/' + id);
  }

  deleteDosage(id: number): Observable<any> {
    return this.http.delete<any>(REST_API + 'dosage/' + id);
  }

  getDosageByMedicationPerPlanId(id: number): Observable<Dosage[]> {
    return this.http.get<Dosage[]>(REST_API + 'dosage/mp/' + id);
  }

}
