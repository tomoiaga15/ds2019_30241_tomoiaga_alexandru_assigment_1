import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Medication} from '../model/Medication';
import {REST_API} from '../common/API';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MedicationService {

  constructor(private http: HttpClient) {
  }

  getMedication(): Observable<Medication[]> {
    return this.http.get<Medication[]>(REST_API + 'medication');
  }

  insertMedication(medication: Medication): Observable<any> {
    return this.http.post<any>(REST_API + 'medication', medication);
  }

  updateMedication(medication: Medication): Observable<Medication> {
    return this.http.put<any>(REST_API + 'medication', medication);
  }

  getMedicationById(id: number): Observable<Medication> {
    return this.http.get<Medication>(REST_API + 'medication/' + id);
  }

  deleteMedication(id: number): Observable<any> {
    return this.http.delete<any>(REST_API + 'medication/' + id);
  }

}
