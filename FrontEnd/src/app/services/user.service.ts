import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {UserView} from '../model/UserView';
import {User} from '../model/User';
import {REST_API} from '../common/API';
import {Observable} from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) {
  }

  getUsers(): Observable<UserView[]> {
    const users = this.http.get<UserView[]>(REST_API + 'user');
    console.log(users);
    return users;
  }

  insertUser(user: User): Observable<number> {
    return this.http.post<any>(REST_API + 'user', user);
  }

  updateUser(user: User): Observable<User> {
    return this.http.put<any>(REST_API + 'user', user);
  }

  getUserById(id: number): Observable<User> {
    return this.http.get<User>(REST_API + 'user/' + id);
  }

  getUserByUsername(username: string): Observable<User> {
    return this.http.get<User>(REST_API + 'user/login/' + username);
  }

  deleteUser(id: number): Observable<any> {
    return this.http.delete<any>(REST_API + 'user/' + id);
  }
}
