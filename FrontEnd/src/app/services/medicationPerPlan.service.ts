import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MedicationPerPlan} from '../model/MedicationPerPlan';
import {REST_API} from '../common/API';
import {Observable} from 'rxjs';
import {Patient} from '../model/Patient';



@Injectable({
  providedIn: 'root'
})
export class MedicationPerPlanService {

  constructor(private http: HttpClient) {
  }

  getMedicationPerPlan(): Observable<MedicationPerPlan[]> {
    return this.http.get<MedicationPerPlan[]>(REST_API + 'medicationperplan');
  }

  insertMedicationPerPlan(medicationPerPlan: MedicationPerPlan): Observable<any> {
    return this.http.post<any>(REST_API + 'medicationperplan', medicationPerPlan);
  }

  updateMedicationPerPlan(medicationPerPlan: MedicationPerPlan): Observable<any> {
    return this.http.put<any>(REST_API + 'medicationperplan', medicationPerPlan);
  }

  getMedicationPerPlanById(id: number): Observable<MedicationPerPlan> {
    return this.http.get<MedicationPerPlan>(REST_API + 'medicationperplan/' + id);
  }

  getMedicationPerPlanByMedicationPlanId(id: number): Observable<MedicationPerPlan[]> {
    return this.http.get<MedicationPerPlan[]>(REST_API + 'medicationperplan/mp/' + id);
  }

}
