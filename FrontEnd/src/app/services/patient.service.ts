import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Patient} from '../model/Patient';
import {REST_API} from '../common/API';
import {Observable} from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class PatientService {

  constructor(private http: HttpClient) {
  }

  getPatient(): Observable<Patient[]> {
    return this.http.get<Patient[]>(REST_API + 'patient');
  }

  insertPatient(patient: Patient): Observable<Patient> {
    return this.http.post<any>(REST_API + 'patient', patient);
  }

  updatePatient(patient: Patient): Observable<Patient> {
    return this.http.put<any>(REST_API + 'patient', patient);
  }

  getPatientById(id: number): Observable<Patient> {
    return this.http.get<Patient>(REST_API + 'patient/' + id);
  }

  getPatientForDoctor(id: number): Observable<Patient[]> {
    return this.http.get<Patient[]>(REST_API + 'patient/doctor/' + id);
  }

  getPatientForCaregiver(id: number): Observable<Patient[]> {
    return this.http.get<Patient[]>(REST_API + 'patient/caregiver/' + id);
  }

  getPatientByUserId(id: number): Observable<Patient> {
    return this.http.get<Patient>(REST_API + 'patient/user/' + id);
  }

  deletePatient(id: number): Observable<any> {
    return this.http.delete<any>(REST_API + 'patient/' + id);
  }
}
