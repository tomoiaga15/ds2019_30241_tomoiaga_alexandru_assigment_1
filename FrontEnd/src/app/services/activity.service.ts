import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {REST_API} from '../common/API';
import {Observable} from 'rxjs';
import {Activity} from '../model/Activity';
import {User} from '../model/User';



@Injectable({
  providedIn: 'root'
})
export class ActivityService {

  constructor(private http: HttpClient) {
  }

  getGraph(s: string): Observable<any> {
    return this.http.get<any>(REST_API + 'activity/graph/' + s);
  }

  findById(id: number): Observable<Activity> {
    return this.http.get<Activity>(REST_API + 'activity/' + id);
  }


  getPills(s: string): Observable<any> {
    return this.http.get<any>(REST_API + 'activity/pills/' + s);
  }

  getActivity(s: string): Observable<any> {
    return this.http.get<any>(REST_API + 'activity/activity/' + s);
  }

  updateNormal(activity: string, id: number): Observable<any> {
    return this.http.put<any>(REST_API + 'activity/normal/' + id, activity);
  }

  updateRecommendation(activity: string, id: number): Observable<any> {
    return this.http.put<any>(REST_API + 'activity/recommend/' + id , activity);
  }

}
