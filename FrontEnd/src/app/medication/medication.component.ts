import { Component, OnInit } from '@angular/core';
import {MedicationService} from '../services/medication.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Medication} from '../model/Medication';

@Component({
  selector: 'app-medication',
  templateUrl: './medication.component.html',
  styleUrls: ['./medication.component.css']
})
export class MedicationComponent implements OnInit {

  medication: Medication;

  constructor(private medicationService: MedicationService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.getMedicationById();
  }

  getMedicationById() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.medicationService.getMedicationById(id)
      .subscribe(medication => {
        this.medication = medication;
      });
  }

  update() {
    this.medicationService.updateMedication(this.medication).subscribe();
  }

  save(name, sideEfect) {
    // tslint:disable-next-line:prefer-const
    let newMedication = new Medication();
    newMedication.name = name;
    newMedication.sideEfect = sideEfect
    this.medicationService.insertMedication(newMedication).subscribe();
  }

  delete(id: number) {
    this.medicationService.deleteMedication(id).subscribe(
      data => {
        this.getMedicationById();
        this.router.navigate(['doctor']);
      }
    );
  }

  goBack() {
    this.router.navigate(['doctor']);
  }
}
