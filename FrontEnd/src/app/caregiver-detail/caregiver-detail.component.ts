import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {PatientService} from '../services/patient.service';
import {Patient} from '../model/Patient';

import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import $ from 'jquery';
import {REST_API} from '../common/API';

@Component({
  selector: 'app-caregiver-detail',
  templateUrl: './caregiver-detail.component.html',
  styleUrls: ['./caregiver-detail.component.css']
})
export class CaregiverDetailComponent implements OnInit {

  patients: Patient[] = [];
  webSocketEndPoint = REST_API + '/websocket'
  client: any;
  // tslint:disable-next-line:max-line-length
  constructor(private route: ActivatedRoute, private router: Router, private patientService: PatientService) { }

  ngOnInit() {
    this.getPatients();
    this.connect();
  }

  getPatients() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.patientService.getPatientForCaregiver(id)
      .subscribe(data => {
      this.patients = data;
    });
  }

  goToPatient(id) {
    this.router.navigate(['patient/' + id]);
  }


  connect() {
    const ws = new SockJS(this.webSocketEndPoint);
    this.client = Stomp.over(ws);
    const that = this;
    // tslint:disable-next-line:only-arrow-functions
    this.client.connect({}, function(frame) {
      that.client.subscribe('/topic/alert', (message) => {
        that.onMessageReceived(message);
      });
    });
  }

  disconnect() {
    if (this.client !== null) {
      this.client.disconnect();
    }
    console.log('Disconnected');
  }

  onMessageReceived(message) {
    console.log('Message Recieved from Server :: ' + message);
    alert(JSON.stringify(message.body));
  }
}
