import { Component, OnInit } from '@angular/core';
import {UserService} from '../services/user.service';
import {ActivatedRoute, Router} from '@angular/router';
import {User} from '../model/User';
import {Role} from '../model/Role';
import {PatientService} from '../services/patient.service';
import {RoleService} from '../services/roleService';
import {Patient} from '../model/Patient';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  user: User;
  doctor: User;
  caregiver: User;

  constructor(private userService: UserService,
              private roleService: RoleService,
              private patientService: PatientService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.getUser();
    this.getDoctor();
    this.getCaregiver();
  }

  getUser() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.userService.getUserById(id)
      .subscribe(user => {
        this.user = user;
      });
  }

  update(id) {
    this.roleService.getRoleById(id)
      .subscribe( data => {
        // @ts-ignore
        // tslint:disable-next-line:radix
        if (this.user.role.id === 3 && parseInt(id) !== 3) {
          this.patientService.getPatientByUserId(this.user.id)
            .subscribe( data1 => {
              this.patientService.deletePatient(data1.id).subscribe( data3 => {
                this.user.role = data;
                this.userService.updateUser(this.user).subscribe(
                  data2 => {
                    this.getUser();
                  });
              });
            });
          // tslint:disable-next-line:radix
        } else if ( parseInt(id) === 3) {
          const newPatient = new Patient();
          newPatient.doctor = this.doctor;
          newPatient.caregiver = this.caregiver;
          this.user.role = data;
          newPatient.user = this.user;
          newPatient.medicalRecord = '';
          this.patientService.insertPatient(newPatient)
            .subscribe( n => {
              console.log(n);
              this.userService.updateUser(this.user).subscribe(
                data2 => {
                  this.getUser();
                });
            });
        }
      });
  }

  getDoctor() {
    this.userService.getUserById(1)
      .subscribe(data => {
        this.doctor = data;
      });
  }

  getCaregiver() {
    this.userService.getUserById(2)
      .subscribe(data => {
        this.caregiver = data;
      });
  }

  delete(id: number) {
    this.userService.deleteUser(id)
      .subscribe(data => {
        this.getUser();
        this.router.navigate(['doctor']);
      });
  }

  goBack() {
    this.router.navigate(['doctor']);
  }

}
