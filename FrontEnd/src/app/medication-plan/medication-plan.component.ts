import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {MedicationPlanService} from '../services/medicationPlan.service';
import {MedicationPerPlanService} from '../services/medicationPerPlan.service';
import {MedicationPlan} from '../model/MedicationPlan';
import {MedicationPerPlan} from '../model/MedicationPerPlan';
import {DosageService} from '../services/dosage.service';
import {MedicationService} from '../services/medication.service';
import {Dosage} from '../model/Dosage';
import {IntakeService} from '../services/intake.service';

@Component({
  selector: 'app-medication-plan',
  templateUrl: './medication-plan.component.html',
  styleUrls: ['./medication-plan.component.css']
})
export class MedicationPlanComponent implements OnInit {

  medicationPlan: MedicationPlan;
  medicationPerPlans: MedicationPerPlan[];
  map = new Map();

  constructor(private medicationPlanService: MedicationPlanService,
              private medicationService: MedicationService,
              private medicationPerPlanService: MedicationPerPlanService,
              private route: ActivatedRoute,
              private intakeService: IntakeService,
              private dosageService: DosageService,
              private router: Router) { }

  ngOnInit() {
    this.getData();
  }

  getData() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.medicationPlanService.getMedicationPlanById(id)
      .subscribe( medicationPlan => {
        this.medicationPlan = medicationPlan;
        this.medicationPerPlanService.getMedicationPerPlanByMedicationPlanId(medicationPlan.id)
          .subscribe(medicationPerPlans => {
            // @ts-ignore
            this.medicationPerPlans = medicationPerPlans;
            // tslint:disable-next-line:prefer-for-of
            for (let i = 0; i < this.medicationPerPlans.length; i++) {
              this.dosageService.getDosageByMedicationPerPlanId(this.medicationPerPlans[i].id)
                .subscribe(data2 => {
                  // @ts-ignore
                  this.map.set(this.medicationPerPlans[i].id, data2);
                });
            }
            // @ts-ignore
            this.medicationPlanService.getMedicationPlanById(this.medicationPerPlans[0].medicationPlan.id)
              .subscribe(data => {
                this.medicationPlan = data;
              });
          });
      });
  }

  update() {
    this.medicationPlanService.updateMedicationPlan(this.medicationPlan)
      .subscribe( data => {
        this.getData();
      });
  }

  getKey(id: number) {
    return this.map.get(id);
  }

  goBack() {
    this.router.navigate(['doctor']);
  }

  addMP(medicationId, intakeId, unit) {
    this.medicationService.getMedicationById(medicationId)
      .subscribe(medication => {
        const newMedicationPerPlan = new MedicationPerPlan();
        newMedicationPerPlan.medication = medication;
        newMedicationPerPlan.medicationPlan = this.medicationPlan;

        this.medicationPerPlanService.getMedicationPerPlanByMedicationPlanId(this.medicationPlan.id)
          .subscribe( medicationPerPlanExist => {
            let ok = false;
            let saveI = 0;
            // tslint:disable-next-line:prefer-for-of
            for (let i = 0; i < medicationPerPlanExist.length && !ok; i++) {
              // @ts-ignore
              // tslint:disable-next-line:triple-equals radix
              if ( medicationPerPlanExist[i].medication.id == parseInt(medicationId)) {
                ok = true;
                saveI = i;
              }
            }
            if (!ok) {
              this.medicationPerPlanService.insertMedicationPerPlan(newMedicationPerPlan)
                .subscribe(medicationPerPlanId => {
                  this.medicationPerPlanService.getMedicationPerPlanById(medicationPerPlanId)
                    .subscribe(medicationPerPlan1 => {
                      const dosage = new Dosage();
                      dosage.unit = unit;
                      dosage.medicationPerPlan = medicationPerPlan1;
                      this.intakeService.getIntakeById(intakeId)
                        .subscribe(intake => {
                          dosage.intake = intake;
                          this.dosageService.getDosageByMedicationPerPlanId(medicationPerPlan1.id)
                            .subscribe(dosages => {
                              let okk = false;
                              for (let ii = 0; ii < dosages.length && !okk; ii++) {
                                // @ts-ignore
                                // tslint:disable-next-line:triple-equals radix
                                if ( dosages[ii].intake.id == parseInt(intakeId)) {
                                  okk = true;
                                }
                              }
                              if (!okk) {
                                this.dosageService.insertDosage(dosage)
                                  .subscribe(data => {
                                    this.getData();
                                  });
                              }
                            });
                        });
                    });
                });
            } else {
              this.medicationPerPlanService.getMedicationPerPlanById(medicationPerPlanExist[saveI].id)
                .subscribe(medicationPerPlan => {
                  const dosage = new Dosage();
                  dosage.unit = unit;
                  dosage.medicationPerPlan = medicationPerPlan;
                  this.intakeService.getIntakeById(intakeId)
                    .subscribe(intake => {
                      dosage.intake = intake;
                      this.dosageService.getDosageByMedicationPerPlanId(medicationPerPlan.id)
                        .subscribe(dosages => {
                          let okk = false;
                          for (let ii = 0; ii < dosages.length && !okk; ii++) {
                            // @ts-ignore
                            // tslint:disable-next-line:triple-equals radix
                            if ( dosages[ii].intake.id == parseInt(intakeId)) {
                              okk = true;
                            }
                          }
                          if (!okk) {
                            this.dosageService.insertDosage(dosage)
                              .subscribe(data => {
                                this.getData();
                              });
                          }
                        });
                    });
                });
            }
        });
      });
  }

  deleteMoment(id) {
    // tslint:disable-next-line:radix
    console.log(parseInt(id));
    // tslint:disable-next-line:radix
    this.dosageService.deleteDosage(parseInt(id))
      .subscribe( data => {
        console.log('muie');
        this.getData();
      });
  }
}
