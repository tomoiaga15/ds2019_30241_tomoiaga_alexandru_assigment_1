export class Dosage {
  id: number;
  intake: object;
  medicationPerPlan: object;
  unit: number;
}
