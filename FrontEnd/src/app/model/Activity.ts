export class Activity {
  id: number;
  patient: object;
  startTime: string;
  endTime: string;
  name: string;
  normal: string;
  recommendations: string;
  time: number;
}
