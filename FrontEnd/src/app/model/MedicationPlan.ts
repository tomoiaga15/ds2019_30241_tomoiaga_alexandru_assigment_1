export class MedicationPlan {
  id: number;
  patient: object;
  startTime: object;
  endTime: object;
}
