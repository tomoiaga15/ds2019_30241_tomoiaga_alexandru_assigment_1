export class Patient {
  id: number;
  user: object;
  doctor: object;
  caregiver: object;
  medicalRecord: string;
}
