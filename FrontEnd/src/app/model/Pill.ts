export class Pill {
  id: number;
  medication: object;
  patient: object;
  intake: object;
  taken: string;
  date: string;
}
