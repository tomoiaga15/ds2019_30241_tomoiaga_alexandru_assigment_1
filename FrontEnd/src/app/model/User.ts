export class User {
  id: number;
  name: string;
  birthdate: string;
  gender: string;
  address: string;
  userName: string;
  password: string;
  role: object;
}
